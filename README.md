Description: Ce programme permet d'afficher des graphiques en fonction de de données météorologiques choisies par l'utilisateur.
Statut du projet: Terminé.
Instructions:
Afin d'exécuter le programme, il est nécessaire d’utiliser unix pour compiler et
exécuter.
Créez un répertoire pour y placer tous les fichiers du projet qui sont présents
dans la branche main du Gitlab.
Utilisez la commande cd <Nom du répertoire> dans le terminal pour vous déplacer
dans le
répertoire.
Tapez ./meteo.sh ainsi que des arguments afin d'executer le programme.
-Si vous voulez afficher une aide pour utiliser le programme et connaître toutes
les options possibles, tapez : ./meteo.sh --help.
L'argument -i vous permet d'identifier le fichier d'entrée, l'argument -o vous permet de nommer le fichier de sortie.
Les arguments -t  et -p  vous afficheront les données respectives des températures et des pressions.
mode 1: Les données seront par ordre croissant de stations et afficheront le minimum, le maximum et la moyenne
mode 2: Affiche les moyennes de toutes les stations par ordre chronologique.
mode 3: Les données seront triées par ordre croissant de station et par ordre chronologique.
L'argument -w affichera les moyennes des données du vent par ordre croissant de station.
L'argument -h affichera les hauteurs par ordre croissant des stations.
L'argument -m affichera les humidités maximales par ordre croissant pour chaque station.
